FROM python:3.10

    RUN mkdir /recuperatorio   

    WORKDIR /recuperatorio

    ADD . /recuperatorio

    RUN pip install --upgrade pip

    RUN pip install -r requerimientos.txt