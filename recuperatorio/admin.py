from django.contrib import admin

from .models import cliente
admin.site.register (cliente)

from .models import producto
admin.site.register (producto)

from .models import detalle
admin.site.register (detalle)

from .models import factura
admin.site.register (factura)



# Register your models here.
