from django.apps import AppConfig


class RecuperatorioConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'recuperatorio'
